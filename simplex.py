#!/usr/bin/env python3

import re
from typing import List, Set, Tuple
from collections import OrderedDict

import sympy as sp


class LinearProblem:
    def __init__(self, goal, objective_func, variables, constraints, assumptions):
        self.goal: str = goal
        self.objective_func: sp.Expr = objective_func
        self.variables: Set[sp.Symbol] = variables
        self.constraints: List[sp.Relational] = constraints
        self.assumptions = assumptions
        # indexes_map = sorted([str(v) for v in variables])
        indexes_map = sorted(list(variables),
                              key=lambda v: (get_index_of_variable(v)[0], len(get_index_of_variable(v)[1])))
        # p = re.compile('[0-9]+p{0,2}')
        # self.indexes_map = [p.search(elem).group() for elem in indexes_map]
        self.indexes_map = [str(get_index_of_variable(elem)[0]) + get_index_of_variable(elem)[1] for elem in indexes_map]

    @classmethod
    def from_string(cls, problem_str):
        problem_str = [line.strip() for line in problem_str.split('\n') if line != '']
        goal = problem_str[0]
        objective_func = sp.sympify(problem_str[1])
        variables = objective_func.free_symbols
        constr_start = problem_str.index('constraints')
        constr_stop = problem_str.index('where')
        constraints = [(sp.sympify(i) if (">" in i or "<" in i) else equation_from_str(i)) for i in
                       problem_str[(constr_start + 1):constr_stop]]
        var_constraints = [sp.sympify(i) for i in problem_str[(constr_stop + 1):]]
        assumptions = [assumption_from_relational(c) for c in var_constraints]
        temp = variables - {list(c.free_symbols)[0] for c in var_constraints}
        assumptions = assumptions + [sp.Q.real(t) for t in temp]
        return cls(goal, objective_func, variables, constraints, assumptions)

    def standarize(self):
        objective_func = self.objective_func
        constraints = []
        assumptions = self.assumptions[:]
        variables = self.variables.copy()
        new_variables = self.variables.copy()
        var_letter = str(list(variables)[0])[0]
        for c in self.constraints:
            new_var = sp.Symbol(var_letter + str(len(new_variables) + 1))
            if rel_operator(c) == '>=':
                # isinstance(c, sp.relational.GreaterThan):
                constraints.append(sp.Eq(c.lhs - new_var, c.rhs))
                new_variables.add(new_var)
                assumptions.append(sp.Q.nonnegative(new_var))
            elif rel_operator(c) == '<=':
                constraints.append(sp.Eq(c.lhs + new_var, c.rhs))
                new_variables.add(new_var)
                assumptions.append(sp.Q.nonnegative(new_var))
            else:
                constraints.append(c)
        with sp.assuming(*assumptions):
            for v in variables:
                if sp.ask(sp.Q.nonpositive(v)):
                    new_var = sp.Symbol(str(v) + "p")
                    constraints = [c.subs(v, -1 * new_var) for c in constraints]
                    assumptions.append(sp.Q.nonnegative(new_var))
                    objective_func = objective_func.subs(v, -1 * new_var)
                    new_variables.remove(v)
                    new_variables.add(new_var)
                elif sp.ask(sp.Q.nonnegative(v)):
                    pass
                elif sp.ask(sp.Q.real(v)):
                    new_var1 = sp.Symbol(str(v) + "p")
                    new_var2 = sp.Symbol(str(v) + "pp")
                    constraints = [c.subs(v, new_var1 - new_var2) for c in constraints]
                    assumptions.append(sp.Q.nonnegative(new_var1))
                    assumptions.append(sp.Q.nonnegative(new_var2))
                    objective_func = objective_func.subs(v, new_var1 - new_var2)
                    new_variables.remove(v)
                    new_variables.add(new_var1)
                    new_variables.add(new_var2)
                else:
                    pass
        # sp.symbols(reply + '1:' + str(n+1))
        return LinearProblem(self.goal, objective_func, new_variables, constraints, assumptions)

    def all_variables(self):
        constraints = []

        objective_func = self.objective_func
        zero_variables_list = list(self.variables - objective_func.free_symbols)
        zero_variables_list = ["0*" + str(v) for v in zero_variables_list]
        if zero_variables_list:
            zero_variables = " + ".join(zero_variables_list)
            objective_func = sp.sympify(str(objective_func) + " + " + zero_variables, evaluate=False)
        for c in self.constraints:
            zero_variables_list = list(self.variables - c.free_symbols)
            zero_variables_list = ["0*" + str(v) for v in zero_variables_list]
            # zero_variables = sp.sympify(' + '.join(zero_variables_list),evaluate=False)
            zero_variables = " + ".join(zero_variables_list)
            tmp = c
            if zero_variables_list:
                if isinstance(c, sp.relational.GreaterThan):
                    tmp = sp.sympify(str(c.lhs) + " + " + zero_variables + " >= " + str(c.rhs), evaluate=False)
                elif isinstance(c, sp.relational.LessThan):
                    tmp = sp.sympify(str(c.lhs) + " + " + zero_variables + " <= " + str(c.rhs), evaluate=False)
                elif isinstance(c, sp.relational.Equality):
                    left = str(c.lhs) + " + " + zero_variables
                    right = str(c.rhs)
                    eq = "Eq(" + left + "," + right + ")"
                    tmp = sp.sympify(eq, evaluate=False)
                else:
                    pass
            constraints.append(tmp)

        return LinearProblem(self.goal, objective_func, self.variables, constraints, self.assumptions)

    def m_methodize(self):
        standarized = self.standarize()
        additional_vars_num = 0
        p = re.compile('.*pp')
        for v in standarized.variables:
            if p.match(str(v)):
                additional_vars_num += 1
        mat = expressions_to_matrix([s.lhs for s in standarized.all_variables().constraints], standarized.variables)
        unit_vects_pos = find_unit_vectors(mat)
        var_letter = str(list(standarized.variables)[0])[0]
        for i in range(len(unit_vects_pos)):
            if not unit_vects_pos[i]:
                new_var = sp.Symbol(var_letter + str(len(standarized.variables) - additional_vars_num + 1))
                standarized.constraints[i] = sp.Eq(standarized.constraints[i].lhs + new_var,
                                                   standarized.constraints[i].rhs)
                standarized.variables.add(new_var)
                standarized.objective_func += sp.Symbol('zM', constant=True) * new_var
        return LinearProblem(self.goal, standarized.objective_func, standarized.variables, standarized.constraints,
                             standarized.assumptions)

    def solve_with_primal(self):
        table = SimplexTable.from_problem(self)
        tables = [table]
        while table.check_if_bounded() and [elem for elem in list(table.deltas) if elem.subs('zM', table.m_value) > 0]:
            # print(table)
            table = table.next_iteration()
            tables.append(table)
        # solution = {}
        # base_ind =
        # # for i in table.unit_vects_indexes:
        #     # solution[] =
        # p = re.compile('([0-9])(+p{0,2})')
        #
        # for i in self.variables:
        #     if p.search(str(i)).group(2):
        #         solution[i] =
        return tables

    def __str__(self):
        return (self.goal + " " + str(self.objective_func) + "\n" + '\n'.join(
            [eq_to_string(c) if "Eq" in str(c) else str(c) for c in self.constraints]) + "\n" + '\n'.join(
            [str(c) for c in self.assumptions]))


class SimplexTable:
    def __init__(self, mat_A, vect_d, vect_c, unit_vects_indexes, indexes_map):
        self.mat_A: sp.Matrix = mat_A
        self.vect_d: sp.Matrix = vect_d
        self.vect_c: sp.Matrix = vect_c
        self.unit_vects_indexes = unit_vects_indexes
        # col_join(),
        self.deltas = []
        self.vect_cb = sp.Matrix([vect_c[0, i] for i in unit_vects_indexes])
        self.deltas.append(vect_d.dot(self.vect_cb))
        for i in range(mat_A.shape[1]):
            self.deltas.append(mat_A.col(i).dot(self.vect_cb) - vect_c[0, i])
        self.deltas = sp.Matrix(self.deltas)
        self.indexes_map = indexes_map
        self.m_value = self.find_m_value()

    @classmethod
    def from_problem(cls, problem: LinearProblem):
        problem = problem.standarize().m_methodize()
        mat_A = expressions_to_matrix([c.lhs for c in problem.all_variables().constraints], problem.variables)
        vect_d = sp.Matrix([c.rhs for c in problem.all_variables().constraints])
        vect_c = expressions_to_matrix([problem.all_variables().objective_func], problem.variables)
        unit_vects_indexes = find_unit_vectors(mat_A)
        indexes_map = problem.indexes_map
        return cls(mat_A, vect_d, vect_c, unit_vects_indexes, indexes_map)

    def build_cell(self, cell_width, content, char):
        return "{char}{0:^{space}}".format(str(content), space=cell_width, char=char)

    def build_row(self, cell_width, content_list, char: str, line_len):
        line = line_len * '='
        output = line + '\n'
        for i in content_list:
            output += self.build_cell(cell_width, i, char)
        output += char + '\n'
        return output

    def find_m_value(self):
        a = [abs(elem) for elem in list(self.mat_A)]
        d = [abs(elem) for elem in list(self.vect_d)]
        c = [abs(elem) for elem in list(self.vect_c) if elem != sp.Symbol('zM')]
        return max(a + d + c) + 10

    def next_iteration(self):
        incoming_index = self.find_incoming_vector_index()
        outgoing_index = self.find_outgoing_vector_index(incoming_index)
        new_base_indexes = tuple([i if i != outgoing_index else incoming_index for i in self.unit_vects_indexes])
        outgoing_pos = [i for (i, j) in enumerate(self.unit_vects_indexes) if j == outgoing_index][0]
        mat_A = self.mat_A.copy()
        vect_d = self.vect_d.copy()
        mat_A.row_op(outgoing_pos, lambda v, j: v / mat_A[outgoing_pos, incoming_index])
        vect_d.row_op(outgoing_pos, lambda v, j: v / self.mat_A[outgoing_pos, incoming_index])

        for i in range(len(new_base_indexes)):
            if i != outgoing_pos:
                value_a = mat_A[i, incoming_index]
                mat_A.row_op(i, lambda v, j: v + mat_A[outgoing_pos, j] * (-1) * value_a)
                vect_d.row_op(i, lambda v, j: v + vect_d[outgoing_pos, 0] * (-1) * value_a)

        return SimplexTable(mat_A, vect_d, self.vect_c, new_base_indexes, self.indexes_map)

    def find_incoming_vector_index(self):
        deltas = [delta.subs('zM', self.m_value) for delta in list(self.deltas)[1:]]
        candidate_indexes = [pair for pair in enumerate(deltas) if pair[0] not in self.unit_vects_indexes
                             and pair[1] > 0]
        index = candidate_indexes[0][0]
        for (ind, val) in candidate_indexes:
            if val > deltas[index]:
                index = ind
        return index

    def find_outgoing_vector_index(self, incoming_index):
        # dzielimy elementy wektora d przez odpowiadające mu dodatnie elementy wektora wchodzącego do bazy
        # i bierzemy z tego minimum. Pobieramy indeks wektora który dał minimum
        # self.unit_vects_indexes[numer elementu z wektora d który dał minimum]
        d = list(self.vect_d)
        incoming_vect = list(self.mat_A.col(incoming_index))
        lambdas = [(ind, val / incoming_vect[ind]) for (ind, val) in enumerate(d) if incoming_vect[ind] > 0]
        index = lambdas[0]
        for (ind, val) in lambdas:
            if val < index[1]:
                index = (ind, val)
        return self.unit_vects_indexes[index[0]]

    def check_if_bounded(self) -> bool:
        sym = sp.Symbol('zM')
        indexes = [i for (i, v) in enumerate(list(self.deltas)) if i != 0
                   and i not in self.unit_vects_indexes
                   and v.subs(sym, self.m_value) > 0
                   and not [elem for elem in list(self.mat_A.col(i-1)) if elem > 0]]
        return not bool(indexes)


    def __str__(self):
        char = '|'
        n_rows = 3 + self.mat_A.shape[0]
        n_columns = 3 + self.mat_A.shape[1]
        cell_width = max([len(str(item)) for item in list(self.mat_A) + list(self.vect_c) + list(self.deltas)]) + 2
        line_len = (cell_width + 1) * n_columns
        row1 = self.build_row(cell_width, ["", "", ""] + list(self.vect_c), char, line_len)
        row2 = self.build_row(cell_width, ["", "", 'z0'] + ['z' + self.indexes_map[i] for i in range(n_columns - 3)],
                              char, line_len)
        row3 = self.build_row(cell_width, ['Nb', 'cB'] + list(self.deltas), char, line_len)
        out = row1 + row2 + row3
        for i in range(self.mat_A.shape[0]):
            lst = [self.indexes_map[self.unit_vects_indexes[i]], self.vect_cb[i, 0], self.vect_d[i, 0]] + list(
                self.mat_A.row(i))
            out += self.build_row(cell_width, lst, char, line_len)
        return out


def rel_operator(rel_expr: sp.relational.Relational) -> str:
    if isinstance(rel_expr, sp.relational.LessThan):
        rel_op = '<='
    elif isinstance(rel_expr, sp.relational.GreaterThan):
        rel_op = '>='
    elif isinstance(rel_expr, sp.relational.StrictLessThan):
        rel_op = '<'
    elif isinstance(rel_expr, sp.relational.StrictGreaterThan):
        rel_op = '>'
    elif isinstance(rel_expr, sp.relational.Unequality):
        rel_op = '!='
    else:
        rel_op = '=='
    return rel_op


def equation_from_str(string):
    sides = [sp.sympify(side, evaluate=False) for side in string.split('=')]
    return sp.Eq(sides[0], sides[1])


def assumption_from_relational(expr):
    variable = list(expr.free_symbols)[0]
    is_nonnegative = expr.subs(variable, 1)
    return sp.Q.nonnegative(variable) if is_nonnegative else sp.Q.nonpositive(variable)


def eq_to_string(eq):
    return str(eq.lhs) + " = " + str(eq.rhs)





def find_unit_vectors(matrix):
    num_of_rows = matrix.shape[0]
    num_of_cols = matrix.shape[1]
    indexes = num_of_rows * [None]
    identity = sp.eye(num_of_rows)
    for i in range(num_of_rows):
        for j in range(num_of_cols):
            if identity.col(i) == matrix.col(j):
                indexes[i] = j
    return tuple(indexes)


def expr_to_vector(expr: sp.Expr, indexed_variables: Set[sp.Symbol]) -> sp.Matrix:
    sym = sp.Symbol('zM')
    # sorted_variables = sorted(list(indexed_variables),
    #                           key=lambda v: (get_index_of_variable(v)[0], len(get_index_of_variable(v)[1])))
    sorted_variables = sorted(list(indexed_variables),
                              key=lambda v: get_index_of_variable(v)[0])
    sorted_variables = sorted(sorted_variables, key=lambda v: len(get_index_of_variable(v)[1]))

    expr_dict = dict((k.subs(sym, 1), sym*v) if k.has(sym) else (k, v) for k, v in expr.as_coefficients_dict().items())
    coefficients_vector = sp.Matrix(1, len(sorted_variables), [expr_dict.get(v, 0) for v in sorted_variables])
    return coefficients_vector


def expressions_to_matrix(expressions, variables):
    matrix = sp.Matrix([expr_to_vector(e, variables) for e in expressions])
    return matrix


def get_index_of_variable(indexed_variable: sp.Symbol) -> Tuple[int, str]:
    number_part = re.search('([0-9]+)(p{0,2})', str(indexed_variable)).group(1)
    letter_part = re.search('([0-9]+)(p{0,2})', str(indexed_variable)).group(2)
    return int(number_part), str(letter_part)

