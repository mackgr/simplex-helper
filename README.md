# Simplex Helper

An application that solves the task of linear programming with the simplex method and shows a step-by-step solution.

## Required non standard Python modules

- SymPy

## Author

Maciej Grabowski