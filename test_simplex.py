#!/usr/bin/env python3
import unittest
from simplex import *
import sympy as sp

expr_str = "7*x1 + 2*x2 + 3*x3 - x4 + x5 + x6 + 5*x8 + 25*x9 - 70*x10"

class LinearProblemTestCase(unittest.TestCase):
    def setUp(self):
        problem_str = """min
2*x1 - 4*x2 + x3 + x4
constraints
x1 - 2*x2 + x3 + 2*x4 >= 6
-x1 + 8*x2 - x3 + x4 <= 4
2*x1 - x2 + 4*x3 + x4 = 8
where
x2 >= 0
x3 >= 0
x4 >= 0"""
        long_input = """min
2*x1 - 4*x2 + x3 + x4 - x5 - x6 - x7 - x8 - x9 - x10
constraints
x1 - 2*x2 + x3 + 2*x4 - x5 - x6 - x7 - x8 - x9 - x10 >= 6
-x1 + 8*x2 - x3 + x4 <= 4
2*x1 - x2 + 4*x3 + x4 = 8
where
x2 >= 0
x3 >= 0
x4 >= 0"""
        self.problem = LinearProblem.from_string(problem_str)
        self.standardized = self.problem.standarize()
        self.m_methodized = self.problem.m_methodize()
    def test_standarized_objective_func(self):
        func = sp.sympify("2*x1p - 2*x1pp - 4*x2 + x3 + x4")
        self.assertEqual(self.standardized.objective_func, func)

    def test_standarized_constraint0(self):
        self.assertEqual(self.standardized.constraints[0], sp.sympify("""Eq(x1p - x1pp - 2*x2 + x3 + 2*x4 - x5,6)"""))

    def test_if_2_times_standardized_problem_not_changed(self):
        standardized2 = self.standardized.standarize()
        self.assertEqual(self.standardized.variables, standardized2.variables)
        self.assertEqual(self.standardized.objective_func, standardized2.objective_func)
        self.assertEqual(self.standardized.constraints, standardized2.constraints)
        self.assertEqual(self.standardized.indexes_map, standardized2.indexes_map)
        self.assertEqual(self.standardized.assumptions, standardized2.assumptions)
        self.assertEqual(self.standardized.goal, standardized2.goal)
    def test_if_2_times_m_methodize_problem_not_changed(self):
        m_methodized2 = self.m_methodized.m_methodize()
        self.assertEqual(self.m_methodized.variables, m_methodized2.variables)
        self.assertEqual(self.m_methodized.objective_func, m_methodized2.objective_func)
        self.assertEqual(self.m_methodized.constraints, m_methodized2.constraints)
        self.assertEqual(self.m_methodized.indexes_map, m_methodized2.indexes_map)
        self.assertEqual(self.m_methodized.assumptions, m_methodized2.assumptions)
        self.assertEqual(self.m_methodized.goal, m_methodized2.goal)

class FunctionsTestCase(unittest.TestCase):
    def test_get_index_of_variable(self):
        symbols = sp.symbols('x1 x15 y3 var5 var255 x2p x3pp x45p x55pp var100p var100pp')
        outputs = [(1,''),(15,''),(3,''),(5,''),(255,''),(2,'p'),(3,'pp'),(45,'p'),(55,'pp'),(100,'p'),(100,'pp')]
        for i in range(len(symbols)):
            self.assertEqual(get_index_of_variable(symbols[i]),outputs[i])

    def test_expr_to_vector(self):
        expr = sp.sympify("7*x1 + 2*x2 + 3*x3 - x4 + x5 + x6 + 5*x8 + 25*x9 - 70*x10 - 15*x1p + 7*x3pp")
        expr2 = sp.sympify("zM*x1 - zM*x2 + x3")
        variables = sp.symbols("x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x1p x3pp")
        variables2 = sp.symbols("x1 x2 x3 x4")
        vector = sp.Matrix([[7, -15, 2, 3, 7, -1, 1, 1, 0, 5, 25, -70, 0, 0]])
        vector2 = sp.Matrix([[sp.Symbol('zM'), -1 * sp.Symbol('zM'), 1, 0]])
        self.assertEqual(vector,expr_to_vector(expr, variables))
        self.assertEqual(vector2, expr_to_vector(expr2, variables2))
unittest.main()
