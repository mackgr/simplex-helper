#!/usr/bin/env python3

import sys
# import os
# import re
# import argparse
import sympy as sp

from simplex import *

def make_frame(string):
    string = str(string)
    line = 80 * '=' + '\n'
    return line + string + '\n' + line
sp.init_printing(use_unicode=True)


if __name__ == "__main__":
    path = sys.argv[1]
    with open(path) as f:
        problem_str = f.read()
    problem = LinearProblem.from_string(problem_str)
    standarized = problem.standarize()
    print(make_frame("original:"))
    print(problem)
    print(make_frame("standarized:"))
    print(standarized)
    print(make_frame("M-method:"))
    print(standarized.m_methodize())
    table = SimplexTable.from_problem(problem)
    print(make_frame("A"))
    sp.pprint(table.mat_A)
    print(make_frame("d"))
    sp.pprint(table.vect_d)
    print(make_frame("c"))
    sp.pprint(table.vect_c)
    print('\n\n\n\n\n')
    for i in problem.solve_with_primal():
        print('\n')
        print(i)

