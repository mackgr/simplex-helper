#!/usr/bin/env python3

from tkinter import *
import tkinter.font as tkfont

import simplex


def solve():
    output_field.configure(state=NORMAL)
    output_field.delete('1.0', END)
    problem_str = input_field.get("1.0", END)
    problem = simplex.LinearProblem.from_string(problem_str)
    standard_form = problem.standarize()
    line = 50 * "=" + "\n"
    all_vars = problem.standarize().all_variables()
    m_method_form = problem.m_methodize()
    solution = problem.solve_with_primal()
    solution_str = "\n\n".join([str(table) for table in solution])
    output_str = "{0}standard form\n{0}{1}\n{0}all vars\n{0}{4}\n{0}M-method form\n{0}{2}\n{3}".format(line,
                                                                                                  str(standard_form),
                                                                                                  str(m_method_form),
                                                                                                  solution_str,
                                                                                                  str(all_vars))
    output_field.insert(END, output_str)
    output_field.configure(state=DISABLED)

def custom_paste(event):
    try:
        event.widget.delete("sel.first", "sel.last")
    except:
        pass
    event.widget.insert("insert", event.widget.clipboard_get())
    return "break"

root = Tk()
root.title("Simplex Helper")
example_input = """min
2*x1 - 4*x2 + x3 + x4
constraints
x1 - 2*x2 + x3 + 2*x4 >= 6
-x1 + 8*x2 - x3 + x4 <= 4
2*x1 - x2 + 4*x3 + x4 = 8
where
x2 >= 0
x3 >= 0
x4 >= 0"""
font = tkfont.Font(family='Consolas', size=12, weight='bold')
# font=("TkFixedFont", 12, "bold")
container = Frame(root, height=80, bg='#ffffff', borderwidth=1, relief="sunken")
scroll_vertical = Scrollbar(container)
scroll_horizontal = Scrollbar(container, orient=HORIZONTAL)

output_field = Text(container, height=50, width=300,wrap=NONE, yscrollcommand=scroll_vertical.set,
                    xscrollcommand=scroll_horizontal.set, borderwidth=0, highlightthickness=0,
                    background="#002A35", foreground="white", font=font, state="disabled")
# for i in range(300):
#     output_field.columnconfigure(i, minsize=10000)


input_field = Text(root, height=20, width=100,wrap=NONE,background="#002A35", foreground="white",
                   font=("Console", 12, "bold"), insertwidth=2, insertbackground="white")
input_field.bind("<<Paste>>", custom_paste)

input_field.insert(END,example_input)
scroll_vertical.config(command=output_field.yview)
scroll_horizontal.config(command=output_field.xview)

scroll_vertical.pack(side=RIGHT, fill=Y)
scroll_horizontal.pack(side=BOTTOM, fill=X)
output_field.pack(side=LEFT, fill=Y)
input_field.pack(side=BOTTOM, fill=Y)

solve_button = Button(root, text="Solve", command=solve)
solve_button.pack(side=BOTTOM)

container.pack()

# var = StringVar()
# label = Label( root, textvariable=var, relief=RAISED )
# var.set(solution)
# label.pack()
# label1 = Label(root, text="Month(MM)")
# E1 = Entry(root, bd =5)

root.mainloop()
